# Projeto Final de Graduação #

## Algoritmos para Isomorfismo de Grafos ##

### Geração de grafos aleatórios ###

Este repositório é um complemento para o Projeto de Final de Graduação sobre Algoritmos para Isomorfismo de Grafos.

Neste repositório está implementado o algoritmo proposto por Mohsen Bayati, Jeong Han Kim e Amin Saberi[^fn1] para geração de grafos aleatório com uma sequência de graus dada.


[^fn1]: Bayati, Mohsen, Jeong Han Kim, and Amin Saberi. "A sequential algorithm for generating random graphs." Algorithmica 58.4 (2010): 860-910.