# -*- coding: utf-8 -*-
"""
Instituto de Computacao - Universidade Estadual de Campinas
Erick Ricardo Mattos
2o semestre de 2016

Este modulo implementa funcoes uteis para a geracao dos
grafos.
"""
# Para metodos numericos
import numpy as np


def grafo2String(G):
    """
        Este metodo transforma um grafo em string, seguindo o
        formato:
            N
            a_11 a_12 ... a_1N
            a_21 a_22 ... a_2N
            .
            .
            .
            a_N1 a_N2 ... a_NN
        Com N numero de vertices do grafo e a_ij eh um valor
        binario:
            1:  Caso a aresta a_ij esta no grafo
            0:  Caso contrario.
    """
    N = len(G)
    retorno = str(N) + "\n"  # Numero de vertices
    for i in range(N):
        for j in range(N):
            retorno += str(G[i, j]) + " "
        retorno += "\n"
    return retorno


def trocaOrdem(g):
    """
        Gera uma grafo permutado de g, ou seja,
        um isomorfismo de g
    """
    # Nuemro de vertices
    n = len(g)
    # Gero uma permmutacao
    novaOrdem = np.random.permutation(n)
    # Retorno o grafo permutado
    return g[:, novaOrdem][novaOrdem, :]
