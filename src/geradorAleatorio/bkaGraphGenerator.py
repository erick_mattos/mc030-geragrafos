# -*- coding: utf-8 -*-
"""
Instituto de Computacao - Universidade Estadual de Campinas
Erick Ricardo Mattos
2o semestre de 2016

Este modulo implementa o algoritmo proposto
    Bayati, Mohsen, Jeong Han Kim, and Amin Saberi.
    "A sequential algorithm for generating random graphs."
    Algorithmica 58.4 (2010): 860-910.
Para geracao de grafos aleatorios dada uma sequencia de
graus
"""
# Para utilizar Arrays implementados em C
import numpy as np
# Para escolher elementos aleatorios com peso
from numpy.random import choice

# Limites de tentativas para uma sequencia de graus
limiteTentativas = 100


def bkaGenerate(d):
    """
        Seja @d uma n-tupla de inteiros que representando a
        seuqencia de graus: a posicao i possui o grau do
        vertice i.
        Retorna a matriz de adjacencia do grafo gerado.
    """
    n = len(d)  # Numero de vertices do grafo
    s = sum(d)
    if s % 2 != 0:
        raise Exception("Nao eh uma sequencia valida de graus:" +
                        " numero nao inteiro de arestas")
    e = s // 2  # Numero de arestas do grafo

    # Encontra as arestas entre todos os vertices
    # Lembrando que um grafo de n vertices pode ter
    # (((n + 1) * n) // 2) arestas: grafo completo
    arestaVetor = np.array(
        [(-1, -1)] * (((n + 1) * n) // 2), dtype=(int, int))
    k = 0
    for i in range(n):
        for j in range(i, n):  # Grafo eh nao direcionado
            arestaVetor[k] = [i, j]
            k += 1
    # Vetor com os indeces do vetor @arestaVetor: necesssario pois
    # a funcao choice nao aceita elementos 2D
    arestaVetorIndex = np.arange(len(arestaVetor), dtype=int)
    numTentativas = 0
    while numTentativas < limiteTentativas:
        numTentativas += 1
        # (1) Start with an empty set E of edges.
        # Matriz de Adjcencia de um grafo sem arestas com @n vertices
        G = np.matrix([[0] * n] * n)
        # Let also d = ( d_1, ..., d_n) be an n-tuple of integers and
        # initialize it by d_ = d
        d_ = np.array(d)

        #(2) Choose two vertices v_i, v_j \in V with probability proportion
        # to d__i*d__j*(1 - (d_i*d_j)/(4*e)) among all pairs i, j with i != j
        # and (v_i, v_j) \notin E. Add (v_i, v_j) to E and reduce each of d__i
        # d__j by 1.
        # Probabilidade inicial
        def probabilidadeAresta(aresta):
            vi, vj = aresta
            # Se a aresta jah foi adicionada, nao a quero novamente
            if G[vi, vj] == 1 or d_[vi] == 0 or d_[vj] == 0 or vi == vj:
                return 0
            return (1 - (d[vi] * d[vj]) / (4 * e)) * (d_[vi] * d_[vj])
        #(3) Repeat step (2) until no more edge can be added to E.
        # calculo as probabilidades
        while True:
            probVetor = np.array(list(map(probabilidadeAresta, arestaVetor)))
            # Garantir que eh probabilidade
            somatoria = np.sum(probVetor)
            if somatoria == 0:
                # print("falha 1")
                break  # Nao tem aresta para escolher
            probVetor = probVetor / np.sum(probVetor)
            # Escolho v_i, v_j com a probabilidade escolhida
            index = np.random.choice(arestaVetorIndex, p=probVetor)
            vi, vj = arestaVetor[index]
            # Com i != j e (v_i, v_j) \notin E
            if vi != vj and G[vi, vj] == 0:
                # Reduza d_i, d_j por 1
                d_[vi] -= 1
                d_[vj] -= 1
                if d_[vi] < 0 or d_[vj] < 0:
                    # Aresta nao deveria existir
                    # print("falha 2")
                    break
                # Adiciono (vi, vj) em G
                G[vi, vj] = 1
                G[vj, vi] = 1  # Grafo nao direcionado
            else:
                # Ate nenhuma aresta poder ser adicionada a E
                # print("falha 3")
                break
        #(4) If |E| < 2*m report failure and restart from step (1), otherwise output G = (V, E).
        if np.sum(G) < 2 * e:
            # Metodo falhou
            return G
        break
    return None
