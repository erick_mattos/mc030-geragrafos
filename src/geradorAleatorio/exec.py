import subprocess

for i in range(70, 101, 10):
    for j in range(20, i//2, 10):
        cmd = ['python', 'main.py']
        cmd.append(str(i))
        cmd.append('--N_INST')
        cmd.append('15')
        cmd.append('--REGULAR')
        cmd.append(str(j))
        cmd.append('--PATH_GRAPH_FILE')
        cmd.append('regulares')
        print(cmd)
        try:
            out = subprocess.check_output(
                cmd, timeout=30*60)
        except subprocess.TimeoutExpired:
            print('error timing', i, j)
            break
