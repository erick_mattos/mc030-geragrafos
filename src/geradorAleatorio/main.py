# -*- coding: utf-8 -*-
"""
Instituto de Computacao - Universidade Estadual de Campinas
Erick Ricardo Mattos
2o semestre de 2016

Este modulo utiliza os demais modulos desenvolvidos para
gerar as instancias dos grafos.
"""
import sys
import os
# Para utilizar Arrays implementados em C
import numpy as np
# Para os argumentos da linha de comando
import argparse
# Para gerar as instancias
import randonGraphGenerator as rgg


# Definicoes
N_INST = 30
PATH_GRAPH_FILE = "./grafo"


def main():
    """
        Funcao que invoca as funcoes para gerar os grafos
    """
    # Argumentos da linha de comando
    parser = argparse.ArgumentParser(
        description='Gerador de grafos aleatorios.')
    parser.add_argument('nVInterval', metavar='N', type=int, nargs='+',
                        help='Numero de vertices do grafo. Pode ser um\
                         intervalo: Start Stop Step')
    parser.add_argument('--N_INST', dest='N_INST', type=int, nargs=1,
                        default=[N_INST],
                        help='Numero de instancias a serem geradas para\
                        cada grafo. Default 30.')
    parser.add_argument('--REGULAR', dest='REGULAR', type=int, nargs=1,
                        default=[0],
                        help='Os grafos gerados serao regulares com\
                        grau igual a REGULAR, 0 indica que nao eh \
                        regular')
    parser.add_argument('--PATH_GRAPH_FILE', dest='PATH_GRAPH_FILE',
                        type=str, nargs=1, default=[PATH_GRAPH_FILE],
                        help='Caminho para onde devem ser salvos os\
                         grafos gerados. Default eh ' +
                        PATH_GRAPH_FILE + '. Sera concatenado a este \
                         caminho informacoes sobre o numero de vertices,\
                         se o grafo eh ou nao regular e a intancia.')
    args = parser.parse_args()
    # Obter o numero de vertices
    nVInterval = []
    if len(args.nVInterval) == 1:
        # Quero apenas grafos com uma quantidade de vertices
        if args.nVInterval[0] > 0:
            nVInterval = np.array(args.nVInterval, dtype=int)
    elif len(args.nVInterval) == 2:
        # Considero Start e Stop, o Step eh 1
        if args.nVInterval[0] > 0:
            nVInterval = np.arange(*(args.nVInterval + [1]))
    elif len(args.nVInterval) == 3:
        # Tenho todos os valores
        if args.nVInterval[0] > 0:
            nVInterval = np.arange(*args.nVInterval)
    if len(nVInterval) == 0:
        print("Erro: intervalo invalido.", file=sys.stderr)
        exit(1)
    nInstancias = int(args.N_INST[0])
    gRegular = int(args.REGULAR[0])
    pathFiles = os.path.abspath(args.PATH_GRAPH_FILE[0]) + "/grafo"
    # Gero as instancias
    gI = rgg.GeraInstancias(nVInterval, nInstancias, gRegular, pathFiles)
    gI.execute()


if __name__ == '__main__':
    main()
