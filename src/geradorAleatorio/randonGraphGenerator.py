# -*- coding: utf-8 -*-
"""
Instituto de Computacao - Universidade Estadual de Campinas
Erick Ricardo Mattos
2o semestre de 2016

Este modulo implementa a geracao de grafos aleatorios.
A cada execucao do algoritmo, sao gerados @N_INST
instancias. Cada instancia eh o conjunto de tres grafos:
dois grafos isomorficos entre si e um nao isomorfico a eles
com uma probabilidade alta.
"""
# Para metodos numericos
import numpy as np
# Para geracao de grafos aleatorios
from bkaGraphGenerator import bkaGenerate
import utils


class GeraInstancias(object):
    """
        Esta classe gera as instancias solicitadas e as
        salvam no disco.
    """

    def __init__(self, nVInterval, nInstancias,
                 gRegular, pathFiles):
        self.nVInterval = nVInterval
        self.nInstancias = nInstancias
        self.gRegular = gRegular
        self.pathFiles = pathFiles

    def execute(self, salvarDisco=True, separador='-' * 15):
        """
            Metodo para gerar as instancias
        """
        for nV in self.nVInterval:
            for numInst in np.arange(self.nInstancias):
                # Gero uma sequencia de graus aleatorias
                if self.gRegular == 0:
                    # Para o nome do arquivo a ser salvo
                    prefixRegular = ''
                    # Nao eh regular: qualquer ordem
                    g1 = g2 = None
                    while g1 is None or g2 is None:
                        # Tento mais esparso para gerar grafos
                        # mais rapidamente
                        degreesSeq = np.random.randint(nV, size=nV)
                        # Se nao for par, nao tenho um grafo valido
                        # pois a soma dos graus dos vertices eh o
                        # dobro do numero de arestas. Logo gero outra
                        # sequencia
                        if sum(degreesSeq) % 2 == 1:
                            continue
                        # Gero dois grafos com esta sequencia
                        # Eles sao, possivelmente, nao isomorficos
                        print("Gerando %2d: %2d" % (nV, numInst + 1))
                        try:
                            g1 = bkaGenerate(tuple(degreesSeq))
                            g2 = bkaGenerate(tuple(degreesSeq))
                        except:
                            g1 = g2 = None
                else:
                    # Para o nome do arquivo a ser salvo
                    prefixRegular = '_R_' + str(self.gRegular).zfill(2)
                    # Se for regular, todos os vertices tem o mesmo
                    # grau
                    degreesSeq = np.array([self.gRegular] * nV)
                    g1 = g2 = None
                    while g1 is None or g2 is None:
                        print("Gerando %2d: %2d" % (nV, numInst + 1))
                        try:
                            g1 = bkaGenerate(tuple(degreesSeq))
                            g2 = bkaGenerate(tuple(degreesSeq))
                        except:
                            g1 = g2 = None

                # Gero um grafo isomorfico a g1, permutando sua
                # matriz de adjacencia
                g1_ = utils.trocaOrdem(g1)
                # Transformo os grafos em strings
                strG1 = utils.grafo2String(g1)
                strG2 = utils.grafo2String(g2)
                strG1_ = utils.grafo2String(g1_)
                # Vejo o formato da saida
                if salvarDisco:
                    # Salvo os grafos em arquivos no disco
                    fileNameG1 = self.pathFiles + prefixRegular + \
                        '_' + str(nV).zfill(4) + \
                        '_' + str(numInst + 1).zfill(2) + \
                        '_' + 'A' + '.grafo'
                    fileNameG1_ = self.pathFiles + prefixRegular + \
                        '_' + str(nV).zfill(4) + \
                        '_' + str(numInst + 1).zfill(2) + \
                        '_' + 'B' + '.grafo'
                    fileNameG2 = self.pathFiles + prefixRegular + \
                        '_' + str(nV).zfill(4) + \
                        '_' + str(numInst + 1).zfill(2) + \
                        '_' + 'C' + '.grafo'
                    with open(fileNameG1, 'w') as f:
                        f.write(strG1)
                        f.close
                    with open(fileNameG1_, 'w') as f:
                        f.write(strG1_)
                        f.close
                    with open(fileNameG2, 'w') as f:
                        f.write(strG2)
                        f.close
                else:
                    # Exibo os grafos na tela
                    print(strG1)
                    print(separador)
                    print(strG1_)
                    print(separador)
                    print(strG2)
                    print(separador)
