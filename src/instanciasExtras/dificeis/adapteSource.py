# -*- coding: utf-8 -*-
"""
Instituto de Computacao - Universidade Estadual de Campinas
Erick Ricardo Mattos
2o semestre de 2016

Este modulo adapta os grafos disponiveis no source para o modelo
adotado no projeto.
"""
import sys, os

# Padrao para o nome dos arquivos gerados
nameFileOut = "grafo_dificil_%02d_%c.grafo"

def readAndSaveMatrix(sourceFile, n, index, id):
    """
        Le a matrix de adjacencia e salva em um arquivo
    """
    strOut = str(n) + '\n'
    # Leio a matrix de adjacencia
    for i in range(n):
        strOut += ' '.join(list(sourceFile.readline()))
    # Salvo no arquivo de saida
    fileOut = open(nameFileOut%(index, id), 'w')
    fileOut.write(strOut)
    fileOut.close()
    # Removo a quebra de linhas subsequente
    sourceFile.readline()
# Caminho para o arquivo source
sourceFilePath = 'source.txt'
# Verifica se argv contem uma path
if len(sys.argv) == 2:
    if os.path.exists(sys.argv[1]):
        nameFileOut = os.path.abspath(os.path.join(sys.argv[1], nameFileOut))

sourceFile = open(sourceFilePath, 'r')
# Indice dos par de grafos
indexGrafos = 1
# Faz a leitura
while True:
    # Le o numero de vertices
    try:
        n = int(sourceFile.readline())
        readAndSaveMatrix(sourceFile, n, indexGrafos, 'A')
        readAndSaveMatrix(sourceFile, n, indexGrafos, 'B')
        indexGrafos += 1
    except:
        break
